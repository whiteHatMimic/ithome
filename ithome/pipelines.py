# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: https://docs.scrapy.org/en/latest/topics/item-pipeline.html


# useful for handling different item types with a single interface

import mysql.connector as mysql;
from itemadapter import ItemAdapter;
from ithome.items import IthomeItem;
from ithome import settings;

class IthomePipeline(object):
    def __init__(self):
        self.connect = mysql.connect(
            host=settings.MYSQL_HOST,
            user=settings.MYSQL_USER,
            password=settings.MYSQL_PASSWORD,
            database=settings.MYSQL_DATEBASE
        );

        if not self.connect.is_connected():
            return 'The mysql connection failed!'
        
        self.cursor = self.connect.cursor();

    def process_item(self, item, spider):
        if item.__class__ == IthomeItem:
            try:
                sql = 'INSERT INTO secuitynews (title, link, images, introduction, date) VALUES (%s, %s, %s, %s, %s)'

                data = (item['title'], item['link'], item['images'], item['introduction'], item['date']);

                self.cursor.execute(sql, data);

                self.connect.commit();
            except Exception as error:
                self.connect.rollback();

            return item;