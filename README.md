<p align="center">
  <h1>iThome 網頁爬蟲</h1>
</p>

<p align="center">
    <a href="https://www.python.org/downloads/release/python-368/">
        <img src="https://img.shields.io/badge/python-3.6.8-blue">
    </a>
    <a href="https://scrapy.org/">
        <img src="https://img.shields.io/badge/scrapy-2.5.0-blue">
    </a>
    <a href="https://downloads.mysql.com/archives/community/">
        <img src="https://img.shields.io/badge/mysql-8.0.25-blue">
    </a>
</p>